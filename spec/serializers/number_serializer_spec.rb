require 'spec_helper'

describe NumberSerializer do
  def expect_serialized(number:, expected_fizz:, expected_buzz:)
    hash = NumberSerializer.new(Fizzbuzz.get(number)).to_h

    expect(hash[:value]).to eq(number)
    expect(hash[:fizz]).to eq(expected_fizz)
    expect(hash[:buzz]).to eq(expected_buzz)
  end

  it { expect_serialized(number: 3, expected_fizz: true, expected_buzz: false) }
  it { expect_serialized(number: 5, expected_fizz: false, expected_buzz: true) }
  it { expect_serialized(number: 15, expected_fizz: true, expected_buzz: true) }
end