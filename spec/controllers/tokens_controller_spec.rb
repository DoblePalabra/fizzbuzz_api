require 'rails_helper'

describe Doorkeeper::TokensController, type: :controller do
  context 'with invalid credentials' do
    it 'returns 401' do
      post :create, params: { grant_type: 'password', username: 'test@test.com', password: 'abcd1234' }
      expect_status(401)
    end
  end

  context 'with a valid user' do
    before { User.create!(email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234') }

    it 'returns a valid token' do
      post :create, params: { grant_type: 'password', username: 'test@test.com', password: 'abcd1234' }
      expect_status(200)
      expect_json_keys(:access_token, :token_type, :expires_in)
    end
  end
end