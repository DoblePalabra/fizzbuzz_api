require 'rails_helper'

describe FavController, type: :controller do
  describe '#index' do
    context 'without a valid session' do
      it 'returns 401' do
        post :index
        expect_status(401)
      end
    end

    context 'with a valid session' do
      let(:user) { User.create!(email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234') }
      let!(:token) { Doorkeeper::AccessToken.create(resource_owner_id: user.id) }
      before { request.headers['Authorization'] = "Bearer #{token.token}" }

      it 'returns a list of favs' do
        Fav.create(user: user, number: 1)
        Fav.create(user: user, number: 3124)
        post :index
        expect_status(200)
        expect_json('numbers.?', number: 1)
        expect_json('numbers.?', number: 3124)
      end
    end
  end

  describe '#create' do
    context 'without a valid session' do
      it 'returns 401' do
        post :create, params: { number: 1 }
        expect_status(401)
      end
    end

    context 'with a valid session' do
      let(:user) { User.create!(email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234') }
      let!(:token) { Doorkeeper::AccessToken.create(resource_owner_id: user.id) }
      before { request.headers['Authorization'] = "Bearer #{token.token}" }

      it 'saves a fav for the user' do
        post :create, params: { number: 1 }
        expect_status(201)
        expect(Fav.where(user: user, number: 1).count).to eq(1)
      end

      # This shouldn't happen with proper use of the API, but I prefer testing against it
      it 'does not save duplicates' do
        Fav.create(user: user, number: 1)
        post :create, params: { number: 1 }
        expect_status(201)
        expect(Fav.where(user: user, number: 1).count).to eq(1)
      end

      it 'can save the same number for another user' do
        user2 = User.create!(email: 'test2@test.com', password: 'abcd1234', password_confirmation: 'abcd1234')
        Fav.create(user: user2, number: 1)

        post :create, params: { number: 1 }
        expect_status(201)
        expect(Fav.where(user: user, number: 1).count).to eq(1)
        expect(Fav.where(user: user2, number: 1).count).to eq(1)
      end
    end
  end

  describe '#destroy' do
    context 'without a valid session' do
      it 'returns 401' do
        post :destroy, params: { number: 1 }
        expect_status(401)
      end
    end

    context 'with a valid session' do
      let(:user) { User.create!(email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234') }
      let!(:token) { Doorkeeper::AccessToken.create(resource_owner_id: user.id) }
      before { request.headers['Authorization'] = "Bearer #{token.token}" }

      context 'without a valid fav' do
        it 'returns 404' do
          post :destroy, params: { number: 1 }
          expect_status(404)
        end
      end

      context 'with a valid fav' do
        it 'destroys the fav' do
          Fav.create(user: user, number: 1)
          post :destroy, params: { number: 1 }
          expect_status(200)
          expect(Fav.where(user: user, number: 1).count).to eq(0)
        end
      end
    end
  end
end