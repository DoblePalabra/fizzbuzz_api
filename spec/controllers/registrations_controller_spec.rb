require 'rails_helper'

describe RegistrationController do
  context 'with valid data' do
    it 'creates a user' do
      user_data = { user: { email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234' } }
      post :create, params: user_data
      expect_status(201)
      expect(User.where(email: 'test@test.com').count).to eq(1)
    end
  end

  context 'with invalid data' do
    it 'does not create a user' do
      user_data = { user: { email: 'test@test.com', password: 'abcd1234', password_confirmation: '1234abcd' } }
      post :create, params: user_data
      expect_status(422)
      expect(User.where(email: 'test@test.com').count).to eq(0)
    end
  end

  context 'when the user already exists' do
    before { User.create!(email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234') }
    it 'does not create a user' do
      user_data = { user: { email: 'test@test.com', password: 'abcd1234', password_confirmation: 'abcd1234' } }
      post :create, params: user_data
      expect_status(422)
      expect(User.where(email: 'test@test.com').count).to eq(1)
    end
  end
end