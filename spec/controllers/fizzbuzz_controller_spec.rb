require 'rails_helper'

describe FizzbuzzController, type: :controller do
  describe 'GET /fizzbuzz' do
    context 'without headers' do
      it 'returns 200' do
        get :index
        expect_status(200)
      end

      it 'returns values from 1 to 100 in order' do
        get :index
        (1..100).each { |value| expect_json("numbers.#{value - 1}", value: value) }
      end

      it 'values divisible by 3 have fizz: true' do
        get :index
        (1..100).each do |value|
          if (value % 3).zero?
            expect_json("numbers.#{value - 1}", fizz: true)
          else
            expect_json("numbers.#{value - 1}", fizz: false)
          end
        end
      end

      it 'values divisible by 5 have buzz: true' do
        get :index
        (1..100).each do |value|
          if (value % 5).zero?
            expect_json("numbers.#{value - 1}", buzz: true)
          else
            expect_json("numbers.#{value - 1}", buzz: false)
          end
        end
      end
    end

    context 'pagination' do
      it 'can change the number of items per page' do
        get :index, params: { per_page: 20 }
        expect_json_sizes(numbers: 20)
      end

      it 'pages start and end on the right values' do
        get :index, params: { page: 1 }
        expect_json('numbers.0', value: 101)
        expect_json('numbers.99', value: 200)
      end

      it 'pages correctly for huge values' do
        get :index, params: { page: 1000, per_page: 1_000 }
        expect_json('numbers.0', value: 1_000_001)
        expect_json('numbers.999', value: 1_001_000)
      end
    end
  end
end