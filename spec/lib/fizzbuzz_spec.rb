require 'spec_helper'

describe Fizzbuzz do
  describe '.get' do
    it '1 returns an object where #fizz? and #buzz? are false' do
      number = Fizzbuzz.get(1)
      expect(number).to_not be_fizz
      expect(number).to_not be_buzz
    end

    it '3 returns an object where #fizz? is true and #buzz? is false' do
      number = Fizzbuzz.get(3)
      expect(number).to be_fizz
      expect(number).to_not be_buzz
    end

    it '5 returns an object where #fizz? is false and #buzz? is true' do
      number = Fizzbuzz.get(5)
      expect(number).to_not be_fizz
      expect(number).to be_buzz
    end

    it '15 returns an object where #fizz? and #buzz? are true' do
      number = Fizzbuzz.get(15)
      expect(number).to be_fizz
      expect(number).to be_buzz
    end
  end
end