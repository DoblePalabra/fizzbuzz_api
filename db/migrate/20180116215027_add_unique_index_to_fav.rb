class AddUniqueIndexToFav < ActiveRecord::Migration[5.1]
  def change
    add_index :favs, [:user_id, :number], unique: true
  end
end
