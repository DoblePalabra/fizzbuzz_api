# README

This repo contains the FizzBuzz API, and also serves a Single Page App that uses it, using React and Webpacker.

## Requirements

* Ruby 2.5
* Yarn
* NodeJS

## How to run the project

* Run bundler

`bundle install`

* Run yarn

`yarn`

* Open the server

`bin/rails server`

The first time will take a bit longer because webpacker has to compile the SPA.
