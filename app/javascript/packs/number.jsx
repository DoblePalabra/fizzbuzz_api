import * as React from "react";
import {Card, Label, Rating} from "semantic-ui-react";

class Number extends React.Component {
    render() {
        return <Card fluid>
            <Card.Content>
                <Card.Header textAlign='center'>{this.props.number}</Card.Header>
                <Card.Content textAlign='center'>
                    { this.fizzbuzz() }
                    { this.rating() }
                </Card.Content>
            </Card.Content>
        </Card>;
    }

    fizzbuzz() {
        if (this.props.fizz && this.props.buzz) {
            return <Label color='red'>FizzBuzz!</Label>;
        }

        if (this.props.fizz) {
            return <Label color='orange'>Fizz!</Label>;
        }

        if (this.props.buzz) {
            return <Label color='blue'>Buzz!</Label>;
        }

        return <div>&nbsp;</div>;
    }

    rating() {
        if (this.props.isLoggedIn) {
            return <Card.Meta textAlign='center'>
                <Rating
                    icon="heart"
                    size="large"
                    rating={this.props.isFav ? 1 : 0}
                    onRate={this.rate.bind(this)}
                />
            </Card.Meta>;
        }

        return <div/>;
    }

    rate() {
        if (this.props.isFav) {
            this.props.onUnFav(this.props.number);
        } else {
            this.props.onFav(this.props.number);
        }
    }
}

export default Number;
