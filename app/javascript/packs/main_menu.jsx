import * as React from "react";
import {Container, Menu} from "semantic-ui-react";
import {Link} from "react-router-dom";
import {observer} from "mobx-react";

@observer
class MainMenu extends React.Component {
    render() {
        return <Menu fixed='top' inverted>
            <Container>
                <Menu.Item as='a' header>
                    FizzBuzz!
                </Menu.Item>

                <Menu.Item>
                    <Link to="/">Main</Link>
                </Menu.Item>
                { this.renderRegistration() }
                }
            </Container>
        </Menu>;
    }

    renderRegistration() {
        if (this.props.appState.isLoggedIn) {
            return <Menu.Item as='a' onClick={() => this.props.appState.logout()}>
                Log out
            </Menu.Item>;
        } else {
            return [
                <Menu.Item>
                    <Link to="/registration">Registration</Link>
                </Menu.Item>,
                <Menu.Item>
                    <Link to="/login">Log in</Link>
                </Menu.Item>
            ];
        }
    }
}

export default MainMenu;
