import * as React from "react";
import {Button, Container, Form, Header, Message} from "semantic-ui-react";
import {observer} from "mobx-react";

@observer
class LoginPage extends React.Component {
    render() {
        const state = this.props.appState.loginForm;

        return <div>
            <Container text style={{ marginTop: '7em' }}>
                <Header content='Login' />
                <Form>
                    <Form.Field>
                        <label>Email</label>
                        <input
                            value={state.email}
                            onChange={(event) => state.setEmail(event.target.value) }
                            placeholder='Email'
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Password</label>
                        <input
                            value={state.password}
                            onChange={(event) => state.setPassword(event.target.value) }
                            type='password'
                            placeholder='Password'
                        />
                    </Form.Field>
                    <Button
                        type='submit'
                        onClick={() => this.props.appState.login()}
                    >
                        Log in!
                    </Button>
                </Form>
                {this.renderErrors()}
            </Container>
        </div>;
    }

    renderErrors() {
        const errors = this.props.appState.loginForm.errors;

        if (errors === null) {
            return;
        }

        return <Message error>
            <Message.Header>There's been some errors!</Message.Header>
            {Object.entries(errors).map(([key, value]) => <Message.Item>{key} - {value}</Message.Item>)}
        </Message>;
    }
}

export default LoginPage;