import * as React from "react";
import {observer} from "mobx-react";
import { Card } from "semantic-ui-react";
import Number from "./number";

@observer
class NumbersList extends React.Component {
    render() {
        return <Card.Group itemsPerRow={5} stackable>
            {this.props.numbers.map((number) =>
                <Number
                    key={number.value}
                    number={number.value}
                    fizz={number.fizz}
                    buzz={number.buzz}
                    isFav={this.isFav(number.value)}
                    onFav={this.props.onFav}
                    onUnFav={this.props.onUnFav}
                    isLoggedIn={this.props.isLoggedIn}
                />)}
        </Card.Group>;
    }

    isFav(number) {
       return this.props.favs.find((fav) => fav.number === number);
    }
}

export default NumbersList;