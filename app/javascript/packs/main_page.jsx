import * as React from "react";
import {Button, Container, Divider, Form, Header, Input, Modal, Pagination} from "semantic-ui-react";
import NumbersList from "./numbers_list";
import {observer} from "mobx-react";

@observer
class MainPage extends React.Component {
    render() {
        return <div>
            <Container text style={{ marginTop: '7em' }}>
                <NumbersList
                    numbers={this.props.appState.numbers}
                    favs={this.props.appState.favs}
                    onFav={(number) => this.props.appState.fav(number)}
                    onUnFav={(number) => this.props.appState.unFav(number)}
                    isLoggedIn={this.props.appState.isLoggedIn}
                />
                <Divider />
                <Input label='Items per page'
                       value={this.props.appState.perPage}
                       onChange={this.changePerPage.bind(this)}/>
                <Divider />
                <Pagination
                    activePage={this.props.appState.page}
                    siblingRange={1}
                    onPageChange={this.changePage.bind(this)}
                    totalPages={100000000000 / this.props.appState.perPage}
                />
            </Container>

        </div>;
    }

    changePage(event, data) {
        this.props.appState.setPage(data.activePage);
    }

    changePerPage(event) {
        this.props.appState.setPerPage(event.target.value);
    }
}

export default MainPage;