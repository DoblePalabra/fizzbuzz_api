// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/index.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import { Router, Route } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import AppState from "./state";
import MainPage from "./main_page";
import MainMenu from "./main_menu";
import RegistrationPage from "./registration_page";
import LoginPage from "./login_page";

class App extends React.Component {
    render() {
        return <div>
            <MainPage appState={this.props.appState} />
        </div>;
    }
}

const appState = new AppState();
appState.history = createBrowserHistory();

document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
        <Router history={appState.history}>
            <div>
                <MainMenu appState={appState} />
                <Route exact path="/" render={() => <App appState={appState} />} />
                <Route exact path="/registration" render={() => <RegistrationPage appState={appState} />} />
                <Route exact path="/login" render={() => <LoginPage appState={appState} />} />
            </div>
        </Router>,
        document.body.appendChild(document.createElement('div')),
    )
});
