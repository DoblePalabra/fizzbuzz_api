import Axios from 'axios';
import {observable, action, computed} from 'mobx';

class AppState {
    history = null;
    @observable page = 1;
    @observable perPage = 100;
    @observable numbers = [];
    @observable favs = [];
    @observable showRegistrationModal = false;
    @observable token = null;
    @observable registrationForm = new RegistrationFormState();
    @observable loginForm = new LoginFormState();

    constructor() {
        this.loadPage();
        this.token = localStorage.getItem("token");
    }

    loadPage() {
        Axios.get(`/fizzbuzz?page=${this.page - 1}&per_page=${this.perPage}`).then((response) => {
            this.setNumbers(response.data.numbers);
        });
    }

    loadFavs() {
        Axios.get('/fav', { headers: { "Authorization": `Bearer ${this.token}`}}).then((response) => {
            this.setFavs(response.data.numbers);
        });
    }

    @action
    setNumbers(numbers) {
        this.numbers = numbers;
    }

    @action
    setFavs(numbers) {
        this.favs = numbers;
    }

    @action
    setPage(page) {
        this.page = page;
        this.loadPage();
    }

    @action
    setPerPage(perPage) {
        this.perPage = perPage;
        this.loadPage();
    }

    @action
    register() {
        this.registrationForm.dismissErrors();
        const payload = {
            user: {
                email: this.registrationForm.email,
                password: this.registrationForm.password,
                password_confirmation: this.registrationForm.passwordConfirmation,
            }
        };

        Axios.post('/registration', payload)
            .then(() => {
                this.history.push("/");
            })
            .catch((error) => {
                this.registrationForm.setErrors(error.response.data.errors);
            })
    }

    @action
    login() {
        this.loginForm.dismissErrors();
        const payload = {
            grant_type: 'password',
            username: this.loginForm.email,
            password: this.loginForm.password
        };

        Axios.post('/oauth/token', payload)
            .then((response) => {
                this.token = response.data.access_token;
                localStorage.setItem("token", this.token);
                this.history.push("/");
                this.loadFavs();
            })
            .catch(() => {
                this.loginForm.setErrors({ credentials: "Username or password not recognised" });
            })
    }

    @action
    logout() {
        this.token = null;
    }

    @action
    fav(number) {
        const headers = { "Authorization": `Bearer ${this.token}`};
        Axios.post('/fav', { number }, { headers }).then(() => {
            this.loadFavs();
        });
    }

    @action
    unFav(number) {
        const headers = { "Authorization": `Bearer ${this.token}`};
        Axios.delete('/fav', { params: { number }, headers }).then(() => {
            this.loadFavs();
        });
    }

    @computed
    get isLoggedIn() {
        return this.token !== null;
    }
}

class RegistrationFormState {
    @observable email = "";
    @observable password = "";
    @observable passwordConfirmation = "";
    @observable errors = null;

    @action
    setEmail(email) {
        this.email = email;
    }

    @action
    setPassword(password) {
        this.password = password;
    }

    @action
    setPasswordConfirmation(password) {
        this.passwordConfirmation = password;
    }

    @action
    setErrors(errors) {
        this.errors = errors;
    }

    @action dismissErrors() {
        this.errors = null;
    }
}

class LoginFormState {
    @observable email = "";
    @observable password = "";
    @observable errors = null;

    @action
    setEmail(email) {
        this.email = email;
    }

    @action
    setPassword(password) {
        this.password = password;
    }

    @action
    setErrors(errors) {
        this.errors = errors;
    }

    @action dismissErrors() {
        this.errors = null;
    }
}

export default AppState;