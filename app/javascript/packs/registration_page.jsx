import * as React from "react";
import {Button, Container, Form, Header, Message} from "semantic-ui-react";
import {observer} from "mobx-react";

@observer
class RegistrationPage extends React.Component {
    render() {
        const state = this.props.appState.registrationForm;

        return <div>
            <Container text style={{ marginTop: '7em' }}>
                <Header content='Registration' />
                <Form>
                    <Form.Field>
                        <label>Email</label>
                        <input
                            value={state.email}
                            onChange={(event) => state.setEmail(event.target.value) }
                            placeholder='Email'
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Password</label>
                        <input
                            value={state.password}
                            onChange={(event) => state.setPassword(event.target.value) }
                            type='password'
                            placeholder='Password'
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Password Confirmation</label>
                        <input
                            value={state.passwordConfirmation}
                            onChange={(event) => state.setPasswordConfirmation(event.target.value) }
                            type='password'
                            placeholder='Password Confirmation'
                        />
                    </Form.Field>
                    <Button
                        type='submit'
                        onClick={() => this.props.appState.register()}
                    >
                        Register!
                    </Button>
                </Form>
                {this.renderErrors()}
            </Container>
        </div>;
    }

    renderErrors() {
        const errors = this.props.appState.registrationForm.errors;

        if (errors === null) {
            return;
        }

        return <Message error>
            <Message.Header>There's been some errors!</Message.Header>
            {Object.entries(errors).map(([key, value]) => <Message.Item>{key} - {value}</Message.Item>)}
        </Message>;
    }
}

export default RegistrationPage;