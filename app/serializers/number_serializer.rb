# Just like the Fizzbuzz class, probably a bit of overengineering for this problem.
# Still, separating controllers from data serialization is usually a quick and big win.
class NumberSerializer
  attr_reader :number

  def initialize(number)
    @number = number
  end

  def to_h
    {
      value: number.value,
      fizz: number.fizz?,
      buzz: number.buzz?
    }
  end
end