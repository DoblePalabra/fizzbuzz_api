class FavsSerializer
  attr_reader :favs

  def initialize(favs)
    @favs = favs
  end

  def to_h
    {
      numbers: favs.map { |fav| { number: fav.number } }
    }
  end
end
