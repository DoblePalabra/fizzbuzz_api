# Class to abstract the rules of Fizzbuzz.
# To be honest, for such a simple problem, it's quite overkill,
# but I want to follow good OOP for this.
class Fizzbuzz
  Number = Struct.new(:value) do
    def fizz?
      (value % 3).zero?
    end

    def buzz?
      (value % 5).zero?
    end
  end

  def self.get(value)
    Number.new(value)
  end
end