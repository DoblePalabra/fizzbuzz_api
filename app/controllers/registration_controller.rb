class RegistrationController < ApplicationController
  def create
    user = User.new(permitted_params[:user])

    if user.save
      render json: {}, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  private

  def permitted_params
    params.permit(user: [:email, :password, :password_confirmation])
  end
end