class FizzbuzzController < ApplicationController
  def index
    numbers = range.map { |value| build_value(value) }
    render json: { numbers: numbers }
  end

  private

  def build_value(number)
    n = Fizzbuzz.get(number)
    NumberSerializer.new(n).to_h
  end

  def range
    per_page = (params[:per_page] || 100).to_i
    page = params[:page].to_i
    start = (page * per_page) + 1
    finish = (start + per_page) - 1
    (start..finish)
  end
end
