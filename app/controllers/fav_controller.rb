class FavController < ApplicationController
  before_action :doorkeeper_authorize!

  def index
    favs = Fav.where(user: current_user)
    serialized = FavsSerializer.new(favs).to_h
    render json: serialized, status: 200
  end

  def create
    fav = Fav.new(user: current_user, number: permitted_params[:number])

    if fav.save
      render json: {}, status: 201
    else
      render json: {}, status: 500
    end

  rescue ActiveRecord::RecordNotUnique
    render json: {}, status: 201
  end

  def destroy
    fav = Fav.find_by(user: current_user, number: permitted_params[:number])

    if fav
      fav.destroy
      render json: {}, status: 200
    else
      render json: {}, status: 404
    end
  end

  private

  def permitted_params
    params.permit(:number)
  end
end