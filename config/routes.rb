Rails.application.routes.draw do
  use_doorkeeper
  root to: 'home#index'
  get '/fizzbuzz', to: 'fizzbuzz#index'
  post '/registration', to: 'registration#create'

  get '/fav', to: 'fav#index'
  post '/fav', to: 'fav#create'
  delete '/fav', to: 'fav#destroy'

  get '*path', to: 'home#index'
end
